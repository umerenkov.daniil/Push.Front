export const environment = {
	production: false,
	vapidPublic: 'BJrdFnCUc7vNErUjhrsyXF2IaCosBN8qYbzIYJe6OFcXRm9S5Wr0RgFgsiPYrFM1psmoRKpjOHj7hgk1WFcSkCk',
	configurationPath: 'http://localhost:5000/api/Configuration/GetCommonSettings',
	localStorageTokenName: 'token'
};