import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/shared/services/data.service';
import { UserService } from 'src/app/shared/services/user.service';
import { LoginRequest } from 'src/app/shared/models/user/login-request.model';
import { ChatService } from 'src/app/shared/services/chat.service';
import { User } from 'src/app/shared/models/user/user.model';
import { Chat } from 'src/app/shared/models/chat/chat.model';

@Component({
    selector: 'push-landing',
    templateUrl: './landing.component.html'
})
export class LandingComponent implements OnInit { 
    public loginRequest: LoginRequest;
    public usersToStartChat: User[];
    public chats: Chat[];
    public sharedDate: Date;

    constructor(
        private _data: DataService,
        public chat: ChatService,
        public user: UserService) { 
        this.loginRequest = new LoginRequest({ email: '1@1.1', password: '123' });
        // this.sharedDate = new Date();
    }

    public ngOnInit(): void {
        this.user.userLoaded.subscribe(() => {
            this._data.getChatAllUsersToStartChat().subscribe(users => this.usersToStartChat = users);
            this._data.getChatAllChats().subscribe(chats => this.chats = chats);
        });

        this._data.getTestLastInserted().subscribe(date => {
            console.warn('get last', date, new Date(date));
            this.sharedDate = new Date(date);
        });
    }

    public startAChat(user: User): void {
        this._data.postChatStartChat(user.id).subscribe(console.warn);    
    }

    public testDate(): void {
        console.warn(this.sharedDate);
        this._data.postTestSaveDateTime(this.sharedDate).subscribe(newDate => {
            this.sharedDate = new Date(newDate);
        });
    }

    public addDate(addDays: number): void {
        let daysToSet = this.sharedDate.getDate() + addDays;
        this.sharedDate.setDate(daysToSet);

        this.sharedDate = new Date(this.sharedDate.toString())
    }

    public trimZone(): void {
        let dateString = this.sharedDate.toJSON();
        console.warn(this.sharedDate.toJSON());
        console.warn(this.sharedDate.toUTCString());
        console.warn(this.sharedDate.toLocaleString());
        console.warn(this.sharedDate.toISOString());
    }
}