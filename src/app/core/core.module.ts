import { NgModule } from '@angular/core';
import { LandingComponent } from './landing/landing.component';
import { CoreRoutes } from './core.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { TestService } from '../shared/services/test.service';
import { ConfigurationService } from '../shared/services/configuration.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MainInterceptor } from '../shared/interceptors/main.interceptor';
import { HttpService } from '../shared/services/http.service';
import { DataService } from '../shared/services/data.service';
import { ChatService } from '../shared/services/chat.service';
import { UserService } from '../shared/services/user.service';
import { FormsModule } from '@angular/forms';
import { AuthInterceptor } from '../shared/interceptors/auth.interceptor';
import { PushService } from '../shared/services/push.service';
import { BsDatepickerModule } from 'ngx-bootstrap';

@NgModule({
    declarations: [
        LandingComponent
    ],
    imports: [
        BrowserAnimationsModule,
        CoreRoutes,
        SharedModule,
        HttpClientModule,
        BsDatepickerModule.forRoot(),
        FormsModule
    ],
    exports: [
        LandingComponent
    ],
    providers: [
        TestService,
        ConfigurationService,
        HttpService,
        ChatService,
        UserService,
        PushService,
        DataService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: MainInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ]
})
export class CoreModule {}