import { NgModule } from '@angular/core';

import { EntryComponent } from './entry.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { CoreModule } from './core/core.module';
import { RouterModule } from '@angular/router';

@NgModule({
	declarations: [
		EntryComponent
	],
	imports: [
		CoreModule,
		RouterModule,
		ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
	],
	providers: [],
	bootstrap: [EntryComponent]
})
export class EntryModule { }
