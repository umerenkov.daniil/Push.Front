import { NgModule } from '@angular/core';
import { SharedComponent } from './components/shared.component';
import { PushChat } from './pages/chat/chat.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BsDatepickerModule } from 'ngx-bootstrap';

@NgModule({
    declarations: [
        SharedComponent,
        PushChat
    ],
    imports: [
        FormsModule,
        BrowserModule
    ],
    exports: [
        SharedComponent,
        PushChat
    ]
})
export class SharedModule {}