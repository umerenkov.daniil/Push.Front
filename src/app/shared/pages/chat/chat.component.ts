import { Component, OnInit, Input } from '@angular/core';
import { Chat } from '../../models/chat/chat.model';
import { DataService } from '../../services/data.service';
import { Message } from '../../models/chat/message.model';
import { ChatMessage } from '../../models/chat/chat-message.model';
import { ChatService } from '../../services/chat.service';
import { filter } from 'rxjs/operators';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'push-chat',
    templateUrl: './chat.component.html'
})
export class PushChat implements OnInit {
    public currentMessage: string;
    public messages: Message[];

    @Input() public chat: Chat;

    constructor(
        private _data: DataService,
        private _user: UserService,
        private _chat: ChatService) {
        this.messages = [];
    }

    public ngOnInit(): void {
        if (!this.chat || !this.chat.id) return;

        this._user.userLoaded.subscribe(() => this._loadMessages());

        this._chat.onMessage
            .pipe(filter(message => message.chatId == this.chat.id))
            .subscribe(message => this.messages.push(new Message({ userId: message.userId, text: message.text })));
    }

    private _loadMessages(): void {
        this._data.getChatAllMessages(this.chat.id).subscribe(messages => this.messages = messages);
    }

    public sendMessage(): void {
        let message = new Message({ text: this.currentMessage });
        this._data.postChatAddMessage(this.chat.id, message).subscribe(() => {
            this.currentMessage = '';
            message.userId = this._user.user.id;
            this.messages.push(message);
        });
    }
}