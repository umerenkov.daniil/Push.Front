import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { LoginRequest } from '../models/user/login-request.model';
import { User } from '../models/user/user.model';
import { map } from 'rxjs/operators';
import { Chat } from '../models/chat/chat.model';
import { Message } from '../models/chat/message.model';
import { PushSubscription } from '../models/push/push-subscription.model';
import { ninvoke } from 'q';

@Injectable()
export class DataService {
    constructor(private _http: HttpService) { }   

    public getUserLogin(request: LoginRequest): Observable<string> {
        return this._http.post<LoginRequest, string>('user/login', request, { stopOnError: false });
    }

    public getUserGetCurrentUser(): Observable<User> {
        return this._http.get<User>('user/getcurrentuser').pipe(map(u => new User(u)));
    }

    public getChatTest(): Observable<string> {
        return this._http.get<string>('chat/Test');
    }

    public getChatAddConnection(connectionId: string): Observable<void> {
        return this._http.get<void>(`chat/addconnection?connectionId=${connectionId}`   );
    }

    public getChatAllChats(): Observable<Chat[]> {
        return this._http.get<Chat[]>('chat/AllChats').pipe(map(chats => chats.map(c => { 
            c.users = c.users.map(p => new User(p));
            return new Chat(c);
        })));
    }

    public getChatAllUsersToStartChat(): Observable<User[]> {
        return this._http.get<User[]>('chat/AllUsersToStartChat').pipe(map(u => u.map(x => new User(x))));
    }

    public postChatStartChat(targetUserId: string): Observable<void> {
        return this._http.post<string, void>('chat/StartChat', targetUserId);
    }

    public postChatAddMessage(chatId: string, message: Message): Observable<void> {
        return this._http.post<Message, void>(`chat/AddMessage?chatId=${chatId}`, message);
    }

    public getChatAllMessages(chatId: string): Observable<Message[]> {
        return this._http.get<Message[]>(`chat/AllMessages?chatId=${chatId}`).pipe(map(m => m.map(msg => new Message(msg))));
    }

    public postPushSaveSubscription(sub: PushSubscription): Observable<void> {
        return this._http.post<PushSubscription, void>('push/SaveSubscription', sub);
    }

    public postTestSaveDateTime(date: Date): Observable<Date> {
        return this._http.post<Date, Date>('test/SaveDateTime', date);
    }

    public getTestLastInserted(): Observable<Date> {
        return this._http.get<Date>('test/LastInserted', null, { stopOnError: false });
    }
}