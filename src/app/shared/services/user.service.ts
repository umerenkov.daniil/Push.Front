import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { LoginRequest } from '../models/user/login-request.model';
import { environment } from '../../../environments/environment';
import { User } from '../models/user/user.model';
import { ReplaySubject } from 'rxjs';
import { ChatService } from './chat.service';

@Injectable()
export class UserService {
    public user: User;
    public userLoaded: ReplaySubject<User>

    constructor(
        private _data: DataService,
        private _chat: ChatService) {

        this.userLoaded = new ReplaySubject<User>(1);
    }

    public tryGetUser(): void {
        let token = this.tryGetToken();
        if (token) this._proceedWithToken(token);
    }

    public login(request: LoginRequest): void {
        this._data.getUserLogin(request).subscribe(token => {
            if (token != null) this._proceedWithToken(token);
        });
    }

    public tryGetToken(): string {
        let token = sessionStorage.getItem(environment.localStorageTokenName);
        return (token == null || token.length == 0) ? null : token;
    } 

    private _proceedWithToken(token: string): void {
        sessionStorage.setItem(environment.localStorageTokenName, token);
        this._data.getUserGetCurrentUser().subscribe(user => {
            this.user = user;
            this.userLoaded.next(user);

            this._chat.infoLoaded.subscribe(() => {
                this._data.getChatAddConnection(this._chat.connectionInfo.connectionId).subscribe();
            });
        });
    }
}