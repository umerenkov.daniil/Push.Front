import { Injectable, OnInit } from '@angular/core';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { ConfigurationService } from './configuration.service';
import { DataService } from './data.service';
import { computeStyle } from '@angular/animations/browser/src/util';
import { ConnectionInfo } from '../models/signalR/connection-info.model';
import { from, ReplaySubject, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ChatMessage } from '../models/chat/chat-message.model';

@Injectable()
export class ChatService implements OnInit {
    public connection: HubConnection;
    public connectionInfo: ConnectionInfo;
    public infoLoaded: ReplaySubject<ConnectionInfo>
    public onMessage: Subject<ChatMessage>;

    constructor(
        private _configuration: ConfigurationService,
        private _data: DataService) {
        this.infoLoaded = new ReplaySubject<ConnectionInfo>(1);
        this.onMessage = new Subject<ChatMessage>();
    }

    public ngOnInit(): void { }

    public start(): void {
        this._configuration.commonLoaded.subscribe(config => {
            this.connection = new HubConnectionBuilder()
                .withUrl(config.chatUrl)
                .build();

            from(this.connection.start()).subscribe(() => this.getConnectionInfo());
            
            this.connection.on('ChatMessage', (payload: ChatMessage) => this.onMessage.next(new ChatMessage(payload)));
        });
    }

    public getCurrentConnection(): void {
        this._data.getChatTest().subscribe(console.warn);
    }
    
    public getConnectionInfo(): void {
        from(this.connection.invoke<ConnectionInfo>('GetConnectionInfo'))
            .pipe(map(info => new ConnectionInfo(info)))
            .subscribe(info => {
                this.connectionInfo = info;
                this.infoLoaded.next(info);
            });
    }
}