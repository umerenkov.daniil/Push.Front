import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiResponse } from '../models/api/api-response.model';
import { map, filter } from 'rxjs/operators';
import { ApiError } from '../models/api/api-error.model';
import { ApiRequestSettings } from '../models/api/api-request-settings.model';

@Injectable()
export class HttpService {
    public get commonHeaders(): HttpHeaders {
        return new HttpHeaders()
            .append('X-Requested-With', 'XMLHttpRequest')
            .append('Content-Type', 'application/json');
    }

    constructor(private _httpClient: HttpClient) {}

    public post<TRequest, TResponse>(url: string, model: TRequest, settings: Partial<ApiRequestSettings> = null): Observable<TResponse> {
        let body = JSON.stringify(model);
        let request = this._httpClient.post<ApiResponse<TResponse>>(url, body, { headers: this.commonHeaders });
        return this._handleStandartRequest(request, settings);
    }

    public get<TResponse, TRequest = any>(url: string, model: TRequest = null, settings: Partial<ApiRequestSettings> = null): Observable<TResponse> {
        let request = this._httpClient.get<ApiResponse<TResponse>>(url, { headers: this.commonHeaders, params: this._getParams(model) })
        return this._handleStandartRequest(request, settings);
    }

    private _getParams<TSource>(source: TSource): HttpParams {
        let params = new HttpParams();
        if (source == null) return params;
        Object.keys(source).forEach((key) => params = params.append(key, source[key]));
        return params;
    }

    private _handleStandartRequest<TResponse>(source: Observable<ApiResponse<TResponse>>, sourceSettings: Partial<ApiRequestSettings>): Observable<TResponse> {
        let settings = ApiRequestSettings.default();
        if (sourceSettings) {
            for (let key in sourceSettings) {
                settings[key] = sourceSettings[key];
            }
        }

        return source.pipe(
            map(resp => {
                if (resp.error != null && settings.handleError) this._handledError(resp.error);
                return resp;
            }),
            filter(val => val.error == null || !settings.stopOnError),
            map(resp => resp.data)
        )
    }
 
    private _handledError(error: ApiError): void {
        console.error('error type', error.type, 'messages', error.messages);
    }
}