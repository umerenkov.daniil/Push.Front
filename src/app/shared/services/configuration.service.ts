import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CommonConfiguration } from '../models/configurations/common.configuration';
import { ReplaySubject } from 'rxjs';
import { ApiResponse } from '../models/api/api-response.model';

@Injectable()
export class ConfigurationService {
    public commonLoaded: ReplaySubject<CommonConfiguration>;
    public configuration: CommonConfiguration;

    constructor(private _httpClient: HttpClient) {
        this.commonLoaded = new ReplaySubject<CommonConfiguration>(1);
    }

    public getCommon(): void {
        let headers = new HttpHeaders()
            .append('X-Requested-With', 'XMLHttpRequest')
            .append('Content-Type', 'application/json')
        
        this._httpClient.get<ApiResponse<CommonConfiguration>>(environment.configurationPath, { headers: headers }).subscribe(conf => {
            this.configuration = new CommonConfiguration(conf.data);
            this.commonLoaded.next(this.configuration);
            this.commonLoaded.complete();
        });
    }
}