import { Injectable } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { DataService } from './data.service';
import { environment } from '../../../environments/environment';
import { from } from 'rxjs';
import { PushSubscription } from '../models/push/push-subscription.model';
import { UserService } from './user.service';

@Injectable()
export class PushService {
    constructor(
        private _user: UserService,
        private _swPush: SwPush,
        private _data: DataService) {

    }

    public trySave(): void {
        if (!this._swPush.isEnabled) {
            console.warn('no push for you, mister');
            return;
        }

        this._swPush.messages.subscribe(console.warn);

        this._user.userLoaded.subscribe(() => {
            from(this._swPush.requestSubscription({ serverPublicKey: environment.vapidPublic }))
                .subscribe(subscribtion => {
                    let sub = PushSubscription.fromServiceWorker(subscribtion.toJSON());
                    this._data.postPushSaveSubscription(sub).subscribe(console.warn);
                });
        });
    }
}