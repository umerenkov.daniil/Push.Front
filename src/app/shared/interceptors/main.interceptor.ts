import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../../environments/environment';
import { ConfigurationService } from '../services/configuration.service';
import { concatMap } from 'rxjs/operators';

@Injectable()
export class MainInterceptor implements HttpInterceptor {
    public constructor(private _configuration: ConfigurationService) {}

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url != environment.configurationPath) {
            let resp = this._configuration.commonLoaded
                .pipe(
                    concatMap(conf => {
                        let cloned = req.clone({ url: `${conf.apiBase}${req.url}` });
                        return next.handle(cloned);
                    })
                )

            return resp;
        }
        else return next.handle(req);
    }
}