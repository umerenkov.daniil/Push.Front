import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { UserService } from '../services/user.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    public constructor(
        private _user: UserService) {}

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let newHeaders = req.headers;
        let authToken = this._user.tryGetToken();

        if (authToken) newHeaders = newHeaders.set('Authorization', `Bearer ${authToken}`)

        let cloned = req.clone({ headers: newHeaders });
        return next.handle(cloned);
    }
}