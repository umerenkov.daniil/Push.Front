export class User {
    public id: string;
    public email: string;

    constructor(init?: Partial<User>) {
        Object.assign(this, init);
    }
}