export class ChatMessage {
    public chatId: string;
    public userId: string;
    public text: string;

    constructor(init?: Partial<ChatMessage>) {
        Object.assign(this, init);
    }
}