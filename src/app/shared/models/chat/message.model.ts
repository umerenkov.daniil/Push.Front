export class Message {
    public userId?: string;
    public text: string;

    constructor(init?: Partial<Message>) {
        Object.assign(this, init);
    }
}