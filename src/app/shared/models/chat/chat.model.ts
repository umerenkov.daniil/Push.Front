import { User } from '../user/user.model';

export class Chat {
    public id: string;
    public created: Date;
    public users: User[];

    constructor(init?: Partial<Chat>) {
        Object.assign(this, init);
    }
}