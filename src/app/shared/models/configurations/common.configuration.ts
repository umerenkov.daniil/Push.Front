export class CommonConfiguration {
    public apiBase: string;
    public chatUrl: string;

    constructor(init?: Partial<CommonConfiguration>) {
        Object.assign(this, init);
    }
}