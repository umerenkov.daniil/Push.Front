import { ApiResponseCode } from './enums/api-response-code.enum';
import { ApiError } from './api-error.model';

export class ApiResponse<TData> {
    public code: ApiResponseCode;
    public error: ApiError;
    public data: TData;

    constructor(init?: Partial<ApiResponse<TData>>) {
        Object.assign(this, init);
    }
}