export enum ApiErrorType {
    AppException = 0,
    UndefinedException = 1
}