export class ApiRequestSettings {
    public stopOnError: boolean;
    public handleError: boolean;

    constructor(init?: Partial<ApiRequestSettings>) {
        Object.assign(this, init);
    }

    public static default(): ApiRequestSettings {
        return new ApiRequestSettings({ 
            stopOnError: true,
            handleError: true
        });
    }
}