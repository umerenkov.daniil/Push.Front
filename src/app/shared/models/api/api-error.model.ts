import { ApiErrorType } from './enums/api-error-type.enum';

export class ApiError {
    public type: ApiErrorType;
    public messages: string[];

    constructor(init?: Partial<ApiError>) {
        Object.assign(this, init);
    }
}