export class ConnectionInfo {
    public connectionId: string;

    constructor(init?: Partial<ConnectionInfo>) {
        Object.assign(this, init);
    }
}