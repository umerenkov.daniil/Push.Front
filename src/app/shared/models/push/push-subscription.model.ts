export class PushSubscription { 
    public endpoint: string;
    public keys: PushSubscriptionKeys;

    constructor(init?: Partial<PushSubscription>) {
        Object.assign(this, init);
    }

    public static fromServiceWorker(source: PushSubscriptionJSON): PushSubscription {
        let data = new PushSubscription();
        data.endpoint = source.endpoint;

        if (!source.keys) return data;

        data.keys = new PushSubscriptionKeys({
            auth: source.keys.auth,
            p256dh: source.keys.p256dh
        });
        return data;
    }
}

export class PushSubscriptionKeys {
    public auth: string;
    public p256dh: string;

    constructor(init?: Partial<PushSubscriptionKeys>) {
        Object.assign(this, init);
    }
}