import { Component, OnInit } from '@angular/core';
import { HttpService } from './shared/services/http.service';
import { ConfigurationService } from './shared/services/configuration.service';
import { ChatService } from './shared/services/chat.service';
import { UserService } from './shared/services/user.service';
import { PushService } from './shared/services/push.service';

@Component({
	selector: 'app-entry',
	templateUrl: './entry.component.html'
})
export class EntryComponent implements OnInit {
	constructor(
		private _configuration: ConfigurationService,
		private _user: UserService,
		private _chat: ChatService,
		private _push: PushService) { }

	public ngOnInit(): void {
		this._configuration.getCommon();
		this._chat.start();
		this._user.tryGetUser();
		this._push.trySave();
	}
}